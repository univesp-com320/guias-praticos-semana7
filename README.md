## Back-End da Aplicação Web em Nuvem Computacional

Neste guia vamos discutir como portar a imagem docker que construímos no guia da Semana 6 para a nuvem computacional da Microsoft Azure. Vamos desenvolver todos esses passos no ambiente Windows 64 bits. 

**NOTA1:** _Caso deseje realizar esta atividade em ambiente Linux, você pode fazê-lo sem problemas, pois basta ter o ambiente docker devidamente instalado e configurado. Você pode rever como fazer isso na disciplina  **COM310 - Infarestrutura para Sistemas de Software - 3o Bimestre.** Lá eu detalho todo o processo de instalação do docker em Linux Ubuntu 20.04. Para fins de funcionalidade, a aplicação que estamos desenvolvendo neste curso vai funcionar tanto com docker no ambiente Linux quanto no Windows._

**NOTA2:** _Você também pode revisitar o material da disciplina  **COM310 - Infarestrutura para Sistemas de Software - 3o Bimestre.** para realizar os mesmos procedimentos para implantar a imagem docker na nuvem da Google. Lá eu detalho todo o processo de configurações que segue o mesmo propósito da nuvem da Microsoft Azure. 

### Pré-Requisitos

Antes de lidarmos com o ambiente da Microsoft Azure, você precisará:

- Ter um conta no [Docker Hub](https://hub.docker.com/) 
- Ter uma conta na [Microsoft Azure](https://azure.microsoft.com/pt-br/free/students/)
- Ter o Azure Cli instalado [Azure Cli](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-windows?tabs=azure-cli)

### Processo de Cadastro de Conta no Docker Hub

Para enviar uma imagem para o Docker Hub você deve ter uma conta já cadastrada lá.

Acesse o site do [Docker Hub](https://hub.docker.com/) para criar uma conta. Siga os procedimentos indicados para a validação de sua conta e após este processo você estará apto para enviar imagens ao Docker Hub.

<p align="center">
  <img target="_blank" src="./imagens/dockerhub-win01.png" alt="Cadastro no docker hub."/>
</p>

#### Login no Docker Hub

Após criar uma nova imagem a partir de uma imagem existente, irei compartilhá-la com todos para facilitar o aprendizado. Desta forma caso vocês tenham tido algum problema com a execução dos guias anteriores, poderá ter a imagem docker funcional que ficará disponível no docker hub. No Power Shell do Windows digite o comando a seguir para fazer o login no Docker Hub:

```
docker login -u <docker-registry-username>
```

Onde <docker-registry-username> é o nome do usuário que você criou no Docker Hub. Você será solicitado a autenticar-se usando sua senha do Docker Hub. Se você especificou a senha correta, a autenticação deve ser bem-sucedida.

#### Salvar a imagem local no Docker Hub 

Vamos partir da imagem que temos do Guia da Semana 6. No meu caso, é por exemplo a imagem:

```
julio/com320-app-v2.0
```

Considerando que você já fez o login no docker hub, o que precisa agora é enviar a imagem para lá. Note que meu login local é julio e co docker hub é juliocest. Deste modo eu terei que etiquetar a imagem local para ser condizente com o usuário juliocest que tenho no docker hub. Abra um PowerShell e digite:

```
docker tag julio/com320-app-v2.0 juliocest/com320-app-v2.0
```

Na sequência digite o comando:

```
docker push juliocest/com320-app-v2.0
```

Espere o processo terminar. Abra um browser, faça login no docker hub e verá a imagem local salva neste repositório remoto!!!

#### Preparação da imagem para a nuvem da Microsoft

A imagem docker que criamos no guia da Semana 6 tem todos os códigos necessários e ferramentas adicionais para o funcionamento do back-end da nossa aplicação Web. Porém, há um detalhe que deixamos para comentar apenas nesta semana. Por padrão, os serviços como Apache e MySQL não iniciam automaticamente quando o container entra em execução e desta forma, se enviarmos esta imagem para a Azure, os serviços MySQL e Apache estarão parados e o front-end não vai conseguir conexão com o back-end. Assim, precisamos realizar os procedimentos abaixo para termos os serviços inicializados automaticamente.

##### Construir uma nova imagem a partir de uma existente

Vamos partir da imagem que enviamos para o Docker Hub. No meu caso é por exemplo a imagem:

```
juliocest/com320-app-v2.0
```

Voce **deve** substituir juliocest pelo nome do seu usuário, conforme vem fazendo nos guias anteriores.

O próximo passo é criar uma imagem nova a partir desta imagem **juliocest/com320-app-v2.0** salva lá no Docker Hub. Para isto vamos fazer uso de um recurso do docker que se chama _dockerfile_. Este arquivo que criaremos vai conter todas as instruções que uma nova imagem docker terá que possuir ao inicializar depois de construída.

Abra o PowerShell e escolha uma pasta em que deseja criar o arquivo dockerfile:

<p align="center">
  <img target="_blank" src="./imagens/dockerhub-win02.png" alt="Pasta para construir nova imagem."/>
</p>

A seguir abra o notepad e adicione o conteúdo abaixo:

<p align="center">
  <img target="_blank" src="./imagens/dockerhub-win03.png" alt="Conteudo do dockerfile."/>
</p>

```
# define a imagem base
FROM juliocest/com320-app-v2.0
# define o mantenedor da imagem
LABEL maintainer="Julio Cezar Estrella"

# Atualiza a imagem com os pacotes
RUN apt-get update && apt-get upgrade -y

# Expoe a porta 80
EXPOSE 80

# Comando para iniciar o apache e o mysql no Container
CMD service apache2 start; service mysql start; tail -f /dev/null
```
<p align="center">
  <img target="_blank" src="./imagens/dockerhub-win04.png" alt="Conteudo do dockerfile."/>
</p>

Salve o arquivo com o nome: **dockerfile**

Ainda no PowerShell e considerando que você está na pasta em que foi salvo o arquivo **dockerfile**, digite o comando a seguir para criar uma nova imagem:

```
docker build -t juliocest/com320-app-v3.0
```

Depois de construída a nova imagem, vamos realizar um teste local para verificar se o Apache e MySQL respondem corretamente, sem termos que executar os comandos de inicialização manualmente, pois na Azure não vamos fazer isso!!!

##### Teste da nova imagem 

No guia da Semana 6 nós inicializamos o docker em modo interativo, passando o argumento **-it** conforme explicado na ocasião. Desta vez vamos iniciar o docker em modo **detach**. O comando abaixo inicia o contêiner, imprime seu id e retorna ao prompt do shell. Assim, podemos continuar com outras tarefas enquanto o contêiner continua em execução em segundo plano.

```
docker run --detach -p 8080:80 juliocest/com320-app-v3.0
```

Abra um browser e digite a URL: **http://localhost:8080** e verá o Laravel em funcionamento conforme a figura a seguir:

<p align="center">
  <img target="_blank" src="./imagens/dockerhub-win05.png" alt="Apache e MySQL em funcionamento no docker."/>
</p>

##### Envio da imagem atualizada - Docker Hub

Para enviar uma imagem ao Docker Hub você deve se manter no Power Shell e digitar o comando a seguir:

```
docker push juliocest/com320-app-v3.0
```
Quando finalizar, uma mensagem como mostrado a seguir deve ser gerada:

```
Using default tag: latest
The push refers to repository [docker.io/juliocest/com320-app-v3.0]
f829b93c1e77: Pushed 
5dff98f42d6f: Mounted from juliocest/com320-app-v3.0 
f457e418d8e7: Mounted from juliocest/com320-app-v3.0 
625f15ae5042: Mounted from juliocest/com320-app-v3.0 
9862644cef89: Mounted from juliocest/com320-app-v3.0 
603a2b58d3d8: Mounted from juliocest/com320-app-v3.0 
9f54eef41275: Mounted from juliocest/com320-app-v3.0 
latest: digest: sha256:4c496e60f7bad0f8b5f27a306d1d1d384ace288fc983b62071f2e37a35c2bf6e size: 1796
```
Espere o processo terminar, acesse o [Docker Hub](https://hub.docker.com/) e verá a imagem local salva neste repositório remoto, como mostra a figura a seguir.

<p align="center">
  <img target="_blank" src="./imagens/dockerhub-win06.png" alt="Nova imagem no dockerhub."/>
</p>

#### Implantação da Imagem e Testes na Microsoft Azure

O primeiro passo é ter uma conta na Azure Cloud. Acesse o link: https://azure.microsoft.com e depois de efetuado o cadastro, você poderá fazer o login e começar a utilizar os serviços da Azure.

Para prosseguir com este guia e não precisar pagar para usar os recursos, você deve acessar o link [Azure for Students](https://azure.microsoft.com/en-us/free/students/) e proceder com os passos para ter uma Conta Free para Estudantes. Neste caso, creio que o email da UNIVESP que cada uma de vocês possui deva funcionar. 

Feito isso, no ambiente da Azure Cloud, sempre que solicitado o Subscription/Assinatura, você deve usar o **Azure for Students**, pois é com essa assinatura que você conseguirá utlizar os créditos gratuitamente por um período de 12 meses.

##### Criar um Registro de Container

Procure por Registro de Container próximo à barra de busca da Azure Cloud e clique nele para criar um repositório no ACR (Azure Container Registry ou em portugues o nome do serviço fica como Registros de Contêiner). Ao clicar no botão criar, teremos a tela como mostrado abaixo.

<p align="center">
  <img target="_blank" src="./imagens/azure-win01.png" alt="Registro de containers da Azure."/>
</p>

<p align="center">
  <img target="_blank" src="./imagens/azure-win02.png" alt="Registro de containers da Azure."/>
</p>

Preencher em:

```
   Grupo de recursos = com320
   Nome do registro = com320
```

O nome do registro também é o nome do repositório. É importante ter isso em mente desde já, pois vamos precisar dessas informações mais adiante. Também é importante lembrar que a localização do seu ACR deve ser, de preferência, a mesma de onde seu cluster Kubernetes irá rodar no futuro.

**NOTA**: _TUDO TEM QUE ESTAR NA MESMA REGIÃO/LOCALIZAÇÃO_

- _(REGISTRO, CLUSTER E RECURSOS)_

Vamos padronizar neste guia que a Região/localização será **Centro-Oeste dos Estados Unidos**.

<p align="center">
  <img target="_blank" src="./imagens/azure-win03.png" alt="Registro de containers da Azure."/>
</p>

Clique em **Review+Create** e terá o registro de containers criado com sucesso, como mostra a figura a seguir.

<p align="center">
  <img target="_blank" src="./imagens/azure-win04.png" alt="Registro de containers da Azure."/>
</p>

##### Execução de comandos no PowerSheel com o Azure Cli

Primeiramente, como indicado no início deste guia, você deve ter baixado e instalado o Azure Cli anteriormente. Feito isso, abra o PowerShell e em seguida digite o comando:

```
az login
```
<p align="center">
  <img target="_blank" src="./imagens/azure-win05.png" alt="Registro de containers da Azure."/>
</p>

<p align="center">
  <img target="_blank" src="./imagens/azure-win06.png" alt="Registro de containers da Azure."/>
</p>

e proceda conforme é solicitado para realizar o login.

<p align="center">
  <img target="_blank" src="./imagens/azure-win07.png" alt="Registro de containers da Azure."/>
</p>

Execute o comando também no PowerShell, passando depois do **name**, o nome do container registry que você cadastrou anteriormente:

<p align="center">
  <img target="_blank" src="./imagens/azure-win08.png" alt="Registro de containers da Azure."/>
</p>

O próximo passo é "etiquetar" a imagem local para um estilo compatível com a Azure. Isso também deve ser feito no PowerShell conforme descrito na imagem a seguir:

<p align="center">
  <img target="_blank" src="./imagens/azure-win09.png" alt="Registro de containers da Azure."/>
</p>
   
Na sequência vamos fazer um **push** da imagem local para a Azure como descrito na imagem abaixo:

<p align="center">
  <img target="_blank" src="./imagens/azure-win10.png" alt="Registro de containers da Azure."/>
</p>

<p align="center">
  <img target="_blank" src="./imagens/azure-win11.png" alt="Registro de containers da Azure."/>
</p>

Para certificarmos que a imagem realmente foi enviada com sucesso à Azure, clique na sequência de setas das imagens a seguir e verifique que o hash que aparece no browser é o mesmo que foi emitido no PowerShell logo após o envio da imagem para a Azure.

<p align="center">
  <img target="_blank" src="./imagens/azure-win13.png" alt="Registro de containers da Azure."/>
</p>
<p align="center">
  <img target="_blank" src="./imagens/azure-win14.png" alt="Registro de containers da Azure."/>
</p>
<p align="center">
  <img target="_blank" src="./imagens/azure-win15.png" alt="Registro de containers da Azure."/>
</p>
<p align="center">
  <img target="_blank" src="./imagens/azure-win16.png" alt="Registro de containers da Azure."/>
</p>

##### Criar o Cluster Kubernetes

Procure por Serviços do Kubernets na barra de busca da Azure. Clique no botão **Criar** e vá na opção **Criar Cluster do Kubernetes.**

<p align="center">
  <img target="_blank" src="./imagens/azure-win17.png" alt="Registro de containers da Azure."/>
</p>
<p align="center">
  <img target="_blank" src="./imagens/azure-win18.png" alt="Registro de containers da Azure."/>
</p>
<p align="center">
  <img target="_blank" src="./imagens/azure-win19.png" alt="Registro de containers da Azure."/>
</p>

Na sequência preencha os campos, colocando o Grupo de recursos como o mesmo nome criado anteriormente para o Registro de Conteiner. Dê o nome do seu cluster e selecione a região.

- Grupo de Recursos: **com320**
- A região deve ser: **Centro-Oeste dos Estados Unidos**
- Registro de Container: **com320**
- Nome do Cluster: **cluster-com320**

Para o tamanho do nó, você pode escolher diversas máquinas. Sugiro que escolha aquela que atenda às suas necessidades. A contagem de nós também fica a critério do usuário. Para esse guia, você pode utilizar a máquina B2s Standard com apenas um nó, pois é uma das mais baratas. Eu utilizei outra, por conta da minha conta na Azure.

<p align="center">
  <img target="_blank" src="./imagens/azure-win21.png" alt="Registro de containers da Azure."/>
</p>
<p align="center">
  <img target="_blank" src="./imagens/azure-win22.png" alt="Registro de containers da Azure."/>
</p>

Você pode dar prosseguimento ao clicar em **Next** (não precisa alterar nada) conforme mostram as imagens na sequência:

<p align="center">
  <img target="_blank" src="./imagens/azure-win23.png" alt="Registro de containers da Azure."/>
</p>
<p align="center">
  <img target="_blank" src="./imagens/azure-win24.png" alt="Registro de containers da Azure."/>
</p>
<p align="center">
  <img target="_blank" src="./imagens/azure-win25.png" alt="Registro de containers da Azure."/>
</p>
<p align="center">
  <img target="_blank" src="./imagens/azure-win26.png" alt="Registro de containers da Azure."/>
</p>

Na próxima figura, clique em **Review+Create** para validar as configurações.

<p align="center">
  <img target="_blank" src="./imagens/azure-win27.png" alt="Registro de containers da Azure."/>
</p>

Se tudo estiver OK, você terá uma tela como mostrada a seguir:

<p align="center">
  <img target="_blank" src="./imagens/azure-win28.png" alt="Registro de containers da Azure."/>
</p>

Conforme a figura a seguir, clique no botão **Create**.

<p align="center">
  <img target="_blank" src="./imagens/azure-win36.png" alt="Registro de containers da Azure."/>
</p>

Assim que a criação do cluster finalizar (pode demorar um pouco), o resultado será mostrado a seguir:

<p align="center">
  <img target="_blank" src="./imagens/azure-win29.png" alt="Registro de containers da Azure."/>
</p>

<p align="center">
  <img target="_blank" src="./imagens/azure-win30.png" alt="Registro de containers da Azure."/>
</p>

Para finalizar clique em **Ir para o recurso**.

<p align="center">
  <img target="_blank" src="./imagens/azure-win37.png" alt="Registro de containers da Azure."/>
</p>

##### Executar seus serviços no Kubernetes

O próximo passo é conectar-se ao Cluster Kubernetes que acabamos de criar. Para isso, acesse o cluster criado no Azure e clique no botão **Conectar** conforme mostrado a seguir.

<p align="center">
  <img target="_blank" src="./imagens/azure-win38.png" alt="Registro de containers da Azure."/>
</p>

<p align="center">
  <img target="_blank" src="./imagens/azure-win39.png" alt="Registro de containers da Azure."/>
</p>

Primeiramente, é necessário realizar uma vinculação do ACR (Azure Container Registry) com o seu AKS (Azure Kubernetes Services). Assim, recomendo utilizar o Azure CLI que foi instalado no Windows. Abra PowerShell do Windows e **copie e cole os dois primeiros comandos** mostrados a seguir. 

<p align="center">
  <img target="_blank" src="./imagens/azure-win40.png" alt="Registro de containers da Azure."/>
</p>

<p align="center">
  <img target="_blank" src="./imagens/azure-win41.png" alt="Registro de containers da Azure."/>
</p>

Após executar os comandos anteriores, ainda no PowerShell, execute o comando listado abaixo:

<p align="center">
  <img target="_blank" src="./imagens/azure-win42.png" alt="Registro de containers da Azure."/>
</p>

E terá a saída destacada na figura a seguir.

<p align="center">
  <img target="_blank" src="./imagens/azure-win43.png" alt="Registro de containers da Azure."/>
</p>

Após a execução do comando anterior, volte ao menu do seu Cluster Kubernetes e clique na aba **Cargas de trabalho ** conforme indicado abaixo:

<p align="center">
  <img target="_blank" src="./imagens/azure-win44.png" alt="Registro de containers da Azure."/>
</p>

Finalizada a criação do cluster, vamos criar nossos arquivos de configuração para serem executados dentro do Kubernetes. É importante pontuar que há diversos recursos dentro do Kubernetes, e que cada recurso é agrupado dentro de um “kind”. Alguns exemplos de kind são _Pod, DaemonSet, Deployments e Service_. O Kubernetes é um universo à parte e vale muito a pena o estudo e aprofundamento sobre seus mais variados tipos de recursos. Entretando, para este guia iremos utilizar apenas dois recursos básicos: **Deployment** e um **Load Balancer Service**.

Com base na imagem a seguir clique em **Adicionar/Adicionar com Yaml**.

<p align="center">
  <img target="_blank" src="./imagens/azure-win45.png" alt="Registro de containers da Azure."/>
</p>

Considere o conteúdo relacionado ao Deployment como mostrado a seguir e cole-o na próxima tela. Este conteúdo é mostrado a seguir:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  replicas: 1
  name: com320-app-v3.0
spec:
  selector:
    matchLabels:
      app: com320-app-v3.0
  template:
    metadata:
      labels:
        app: com320-app-v3.0
    spec:
      containers:
        - name: com320-app
          image: com320.azurecr.io/com320-app-v3.0
          imagePullPolicy: Always
          ports:
-containerPort: 80
```

Depois de copiar e colocar, clique em **Adicionar**.

<p align="center">
  <img target="_blank" src="./imagens/azure-win46.png" alt="Registro de containers da Azure."/>
</p>

Após finalizado, o resultado é confirmado com base nas setas indicadas na imagem abaixo.

<p align="center">
  <img target="_blank" src="./imagens/azure-win48.png" alt="Registro de containers da Azure."/>
</p>

##### Permitindo acesso externo ao seus serviços no Kubernetes

Infelizmente, ao subir um serviço do tipo Deployment, não é possível acessar o serviço de forma externa. Para que isso ocorra, é necessário a criação de algum serviço do Kubernetes que efetue essa conexão. Alguns exemplos de serviços que podem executar essa ação são **NodePorts** e **LoadBalancer**, já que ambos possuem a capacidade de conectar “pods”, os quais são seus contêineres, com o ambiente externo.

Assim, criaremos um **Load Balancer** que, além de permitir o acesso externo de seus containers, realiza também a distribuição de carga entre eles. Para isso, abra o menu **Serviços e Entradas** e clique no botão **Adicionar/Adicionar com Yaml**.

<p align="center">
  <img target="_blank" src="./imagens/azure-win49.png" alt="Registro de containers da Azure."/>
</p>

<p align="center">
  <img target="_blank" src="./imagens/azure-win50.png" alt="Registro de containers da Azure."/>
</p>

Copie e cole todo o conteúdo descrito a seguir no campo indicado e clique no botão **Adicionar** ao final.

```
apiVersion: v1
kind: Service
metadata:
  name: lb-com320-app
spec:
  type: LoadBalancer
  selector:
    app: com320-app-v3.0
  ports:
    - protocol: TCP
      port: 8080
      targetPort: 80
```
<p align="center">
  <img target="_blank" src="./imagens/azure-win51.png" alt="Registro de containers da Azure."/>
</p>

O resultado da adição é mostrado a seguir, com a criação do balanceador de carga e a emissão de um link externo para que possamos acessar o back-end configurado dentro da imagem docker que construímos na **Semana 6** e que foi enviada à nuvem da Azure.

<p align="center">
  <img target="_blank" src="./imagens/azure-win52.png" alt="Registro de containers da Azure."/>
</p>

Clique na seta indicada ou copie o endereço de IP disponível, abra um browser e complemente ao final do endereço IP, com o número da porta 8080. 

**_Observação: Esse endereço IP não será o mesmo que eu indico aqui, pois ele é gerado aleatoriamente pela Azure. Você deve usar o endereço IP que será fornecido para você quando finalizar esta etapa, e acrescentar ao final dele a porta 8080._**

O resultado final indica que temos a imagem Linux em execução com Apache, PHP e Laravel em funcionamento.

<p align="center">
  <img target="_blank" src="./imagens/azure-win53.png" alt="Registro de containers da Azure."/>
</p>

#### Teste do Front-End com o Back-End na Azure

Chegamos ao teste final em que mostraremos a seguir o acesso do front-end local ao back-end na nuvem da Azure. O primeiro passo é executar o ambiente laragon, pois por meio do terminal que ele disponibiliza poderemos iniciar a aplicação angular-crud-app (desenvolvida na Semana 3). 

<p align="center">
  <img target="_blank" src="./imagens/azure-win54.png" alt="Execução do laragon."/>
</p>

Acesse a pasta conforme indicado na imagem abaixo:

<p align="center">
  <img target="_blank" src="./imagens/azure-win55.png" alt="Acesso ao angular-crud-app."/>
</p>

Percorra o diretório **src/environments** e abra o arquivo _**environment.ts**_.

<p align="center">
  <img target="_blank" src="./imagens/azure-win56.png" alt="Acesso ao arquivo environment.ts."/>
</p>

Precisamos alterar uma linha que indica a URL do back-end. No meu caso é o IP que já discuti anteriormente. **No seu caso, será o IP que a Azure fornecerá para você.**

<p align="center">
  <img target="_blank" src="./imagens/azure-win57.png" alt="Alteração do arquivo environment.ts."/>
</p>

Inicie a aplicação **angular-crud-app** conforme indicado na seta da figura a seguir:

<p align="center">
  <img target="_blank" src="./imagens/azure-win58.png" alt="Inicia a aplicação."/>
</p>

E o resultado da compilação é descrito abaixo:

<p align="center">
  <img target="_blank" src="./imagens/azure-win59.png" alt="Resultado da compilação."/>
</p>

O próximo passo é abrir um browser e navegar pela aplicação, realizando as ações como inserções, remoções, edições, etc. 

Acesso ao painel principal.

<p align="center">
  <img target="_blank" src="./imagens/azure-win60.png" alt="Resultado da compilação."/>
</p>

Acesso à api pais.

<p align="center">
  <img target="_blank" src="./imagens/azure-win61.png" alt="Resultado da compilação."/>
</p>

Acesso à api universidades.
<p align="center">
  <img target="_blank" src="./imagens/azure-win62.png" alt="Resultado da compilação."/>
</p>

#### Conclusão

Como docente responsável pela disciplina, tentei ao longo das semanas trazer para todos uma abordagem mais prática dos assuntos tratados na parte teórica. Entendo que não é simples assimilar todos os conteúdos apresentados em tão pouco tempo. Porém eu espero que esses guias sirvam de pontapé incial para vocês procurarem mais materiais, praticarem bastante e em breve tornarem-se desenvolvedores competentes de front-end, back-end e porque não full-stack. 
